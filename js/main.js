var startDate=new Date(2014, 6, 1);
var endDate=new Date(2014, 6, 30);
var date=new Date(2014, 6, 20);

var points = new Array();

points[0]={data:new Date(2014, 6, 1), nazwa:"Punkt 1 Punkt 1 Punkt 1 Punkt 1", ikona:"heart-full-outline"};
points[1]={data:new Date(2014, 6, 12), nazwa:"Punkt 2", ikona:"phone-outline"};
points[2]={data:new Date(2014, 6, 18), nazwa:"Punkt 3", ikona:"tree"};
points[3]={data:new Date(2014, 6, 24), nazwa:"Punkt 4", ikona:"cog-outline"};

function tmpl(point){
    var tmpl='<li class="icon';
    if(point['data']>date){
        tmpl+=' future';
    }
    return tmpl+'" style="left:'+Math.round(100*(point['data'].getTime()-startDate.getTime())/(endDate.getTime()-startDate.getTime()))+'%">'+
        '<div class="circle gradient"><span class="typcn typcn-'+point['ikona']+'"></span></div>'+
        '<div class="info">'+
            '<h3>'+point['data'].getDate()+'-'+point['data'].getMonth()+'-'+point['data'].getFullYear()+'</h3>'+
            '<hr>'+
                '<h2>'+point['nazwa']+'</h2>'+
            '</div>'+
        '</li>'
}

var list = $('#timeline ul');

$('#timeline .bar .progress').css('width',Math.round(100*(date.getTime()-startDate.getTime())/(endDate.getTime()-startDate.getTime()))+'%');

points.forEach(function(entry) {
    list.append(tmpl(entry));
});
